/**
 * @author Jeffrey Crowell
 *
 */


import java.util.ArrayList;
import java.util.Random;
public class Node
{
	Board board;
	boolean isMax = true;

	/**
	 * Creates a node from a previous instance
	 * @param board the parent board
	 * @param x the X location of the move
	 * @param y the Y location of the move
	 * @param z the Z location of the move
	 * @param color the Color of the move
	 */
	public Node(Board board, int x, int y, int z, int color)
	{
		this.board = new Board(board, x, y, z, color);
	}

	/**
	 * Creates a Node from a string(the given from the game)
	 * @param board the string representing the board
	 */
	public Node(String board)
	{
		this.board = new Board(board);
	}

	public ArrayList<Node> getChildren(boolean Player)
	{
		int RED = 1;
		int GREEN = 2;
		int BLUE = 3;
		ArrayList<Node> children = new ArrayList<Node>();
		if(board.prevMove.size() == 0)
		{
			for(int ii = 1; ii < board.board.size() - 1; ii++)
			{
				int thisX = ii;
				int Y1 = 1;
				int Z1 = board.board.get(ii).size() - 2;
				int Y2 = Z1;
				int Z2 = Y1;
				children.add(new Node(this.board, thisX, Y1, Z1, RED));
				children.add(new Node(this.board, thisX, Y1, Z1, GREEN));
				children.add(new Node(this.board, thisX, Y1, Z1, BLUE));
				children.add(new Node(this.board, thisX, Y2, Z2, RED));
				children.add(new Node(this.board, thisX, Y2, Z2, GREEN));
				children.add(new Node(this.board, thisX, Y2, Z2, BLUE));
			}
			for(int ii = 2; ii < board.board.get(1).size() - 2; ii++)
			{
				//now add the rest of the bottom board
				int thisX = 1;
				int Y1 = ii;
				int Z1 = board.board.get(1).size() - Y1 - 1;
				children.add(new Node(this.board, thisX, Y1, Z1, RED));
				children.add(new Node(this.board, thisX, Y1, Z1, GREEN));
				children.add(new Node(this.board, thisX, Y1, Z1, BLUE));
			}
		}
		else
		{
			//create subtree and return results
			//first find where possible moves can be
			//int color = board.prevMove.get(1);  //never actually used :/
			int x = board.prevMove.get(1);
			int y = board.prevMove.get(2);
			int z = board.prevMove.get(3);
			int boardSize = board.board.size();

			//SAME and to the LEFT
			int xSame_1 = x;
			int ySame_1 = y-1;
			int zSame_1 = z+1;
			if(this.board.board.get(xSame_1).get(ySame_1) == 0)
			{
				//it is uncolored! add it to the children
				children.add(new Node(this.board, xSame_1, ySame_1, zSame_1, RED));
				children.add(new Node(this.board, xSame_1, ySame_1, zSame_1, GREEN));
				children.add(new Node(this.board, xSame_1, ySame_1, zSame_1, BLUE));

			}
			//SAME and to the RIGHT
			int xSame_2 = x;
			int ySame_2 = y+1;
			int zSame_2 = z-1;
			if(this.board.board.get(xSame_2).get(ySame_2) == 0)
			{
				//it is uncolored! add it to the children
				children.add(new Node(this.board, xSame_2, ySame_2, zSame_2, RED));
				children.add(new Node(this.board, xSame_2, ySame_2, zSame_2, GREEN));
				children.add(new Node(this.board, xSame_2, ySame_2, zSame_1, BLUE));

			}
			//ABOVE and to the LEFT
			int xAbove_1 = x+1;
			int yAbove_1 = y-1;
			int zAbove_1 = z;
			if(this.board.board.get(xAbove_1).get(yAbove_1) == 0)
			{
				//it is uncolored! add it to the children
				children.add(new Node(this.board, xAbove_1, yAbove_1, zAbove_1, RED));
				children.add(new Node(this.board, xAbove_1, yAbove_1, zAbove_1, GREEN));
				children.add(new Node(this.board, xAbove_1, yAbove_1, zAbove_1, BLUE));

			}
			//ABOVE and to the RIGHT
			int xAbove_2 = x + 1;
			int yAbove_2 = y;
			int zAbove_2 = z - 1;
			if(this.board.board.get(xAbove_2).get(yAbove_2) == 0)
			{
				//it is uncolored! add it to the children
				children.add(new Node(this.board, xAbove_2, yAbove_2, zAbove_2, RED));
				children.add(new Node(this.board, xAbove_2, yAbove_2, zAbove_2, GREEN));
				children.add(new Node(this.board, xAbove_2, yAbove_2, zAbove_2, BLUE));

			}

			if(x != 1) //not on the bottom row
			{
				//BELOW and to the LEFT
				int xBelow_1 = x - 1;
				int yBelow_1 = y;
				int zBelow_1 = z+1;
				if(this.board.board.get(xBelow_1).get(yBelow_1) == 0)
				{
					//it is uncolored! add it to the children
					children.add(new Node(this.board, xBelow_1, yBelow_1, zBelow_1, RED));
					children.add(new Node(this.board, xBelow_1, yBelow_1, zBelow_1, GREEN));
					children.add(new Node(this.board, xBelow_1, yBelow_1, zBelow_1, BLUE));

				}
				//BELOW and to the RIGHT
				int xBelow_2 = x - 1;
				int yBelow_2 = y + 1;
				int zBelow_2 = z;
				if(this.board.board.get(xBelow_2).get(yBelow_2) == 0)
				{
					//it is uncolored! add it to the children
					children.add(new Node(this.board, xBelow_2, yBelow_2, zBelow_2, RED));
					children.add(new Node(this.board, xBelow_2, yBelow_2, zBelow_2, GREEN));
					children.add(new Node(this.board, xBelow_2, yBelow_2, zBelow_2, BLUE));

				}

			}

			if(children.size() == 0)
			{
				//it was impossible to add a good move.  now go through and find any open square
				for(int ii = 0; ii < board.board.size(); ii++)
				{
					for(int jj = 0; jj < board.board.get(ii).size(); jj++)
					{
						if(board.board.get(ii).get(jj) == 0)
						{
							//the square is open
							int zz = board.board.get(ii).size() - jj - 1;
							children.add(new Node(this.board, ii, jj, zz, RED));
							children.add(new Node(this.board, ii, jj, zz, GREEN));
							children.add(new Node(this.board, ii, jj, zz, BLUE));
						}
					}
				}
			}
		}

		return children;
	}

	public boolean finished(int color1, int color2, int color3)
	{
		if(color1 == 0 || color2 == 0 || color3 == 0)
		{
			return false;
		}
		else if(color1 != color2 && color1 != color3 && color2 != color3)
		{
			return true;
		}
		return false;
	}
	/**
	 * Checks to see if this is a terminating move
	 * @param player Max Player = true
	 * @return true if is a terminal move
	 */
	public boolean isFinal(boolean player)
	{
		boolean finalNode = false;
		//check to see if the game can be over
		//first check up and above
		int thisX = board.prevMove.get(1);
		int thisY = board.prevMove.get(2);

		int thisColor = board.prevMove.get(0);
		int sameLeftColor = board.board.get(thisX).get(thisY - 1); //same level left
		int sameRightColor = board.board.get(thisX).get(thisY + 1); //same level right
		int upLeftColor = board.board.get(thisX + 1).get(thisY - 1); //upper left
		int upRightColor = board.board.get(thisX + 1).get(thisY); //upper right
		int btLeftColor;
		int btRightColor;
		if(thisX == 1)
		{
			//we're on the second to last row, btLeft and right are different
			btLeftColor = board.board.get(thisX - 1).get(thisY - 1); //bottom left
			btRightColor = board.board.get(thisX - 1).get(thisY);  //bottom right
		}
		else
		{
			btLeftColor = board.board.get(thisX - 1).get(thisY); //bottom left
			btRightColor = board.board.get(thisX - 1).get(thisY + 1); //bottom right
		}

		//got all of the colors, now do the checks for finishing move
		if(finished(thisColor, sameLeftColor, upLeftColor) ||
				finished(thisColor, sameRightColor, upRightColor) ||
				finished(thisColor, sameLeftColor, btLeftColor) ||
				finished(thisColor, sameRightColor, btRightColor) ||
				finished(thisColor, upLeftColor, upRightColor) ||
				finished(thisColor, btLeftColor, btRightColor))
		{
			finalNode = true;
		}


		return finalNode;
	}
	/**
	 * Static Evaluator
	 * @param Player boolean true = MaxPlayer
	 * @return the Static Evaluation
	 */
	public int getScore(boolean Player, boolean finishingMove)
	{
		int score = 0;

		//do the heuristic eval to return the score
		//if finishing and maxplayer, return -inf
		if(Player && finishingMove)
		{
			return -999999;
		}
		//if finishing and minplayer return +inf
		else if(!Player && finishingMove)
		{
			return 999999;
		}

		int [] color = new int [3];
		color[0] = this.board.prevMove.get(0);
		int max = 0;
		int temp = 0;

		int thisX = board.prevMove.get(1);
		int thisY = board.prevMove.get(2);

		int thisColor = board.prevMove.get(0);
		int sameLeftColor = board.board.get(thisX).get(thisY - 1); //same level left
		int sameRightColor = board.board.get(thisX).get(thisY + 1); //same level right
		int upLeftColor = board.board.get(thisX + 1).get(thisY - 1); //upper left
		int upRightColor = board.board.get(thisX + 1).get(thisY); //upper right
		int btLeftColor;
		int btRightColor;
		if(thisX == 1)
		{
			//we're on the second to last row, btLeft and right are different
			btLeftColor = board.board.get(thisX - 1).get(thisY - 1); //bottom left
			btRightColor = board.board.get(thisX - 1).get(thisY);  //bottom right
		}
		else
		{
			btLeftColor = board.board.get(thisX - 1).get(thisY); //bottom left
			btRightColor = board.board.get(thisX - 1).get(thisY + 1); //bottom right
		}

		//now check each of the possible moves
		for(int ii = 0; ii < 6; ii++)
		{
			switch(ii)
			{
			case 0:
				color[1] = sameLeftColor;
				color[2] = upLeftColor;
				break;
			case 1:
				color[1] = upLeftColor;
				color[2] = upRightColor;
				break;
			case 2:
				color[1] = upRightColor;
				color[2] = sameRightColor;
				break;
			case 3:
				color[1] = sameRightColor;
				color[2] = btRightColor;
				break;
			case 4:
				color[1] = btRightColor;
				color[2] = btLeftColor;
				break;
			case 5:
				color[1] = btLeftColor;
				color[2] = sameLeftColor;
				break;
			}


			Random rand = new Random();

			//two empty spots, so, its not so good or so bad
			if(color[1] == 0 && color[2] == 0)
			{
				temp = (rand.nextInt() % 3) + 10;
				if(temp > max)
				{
					max = temp;
				}
			}
			//only one empty space around the current move
			else if(color[1] == 0)
			{
				if(color[0] != color[2])
				{
					//they're different, so its better!
					temp = (rand.nextInt() % 3) + 100;
				}
				else
				{
					//they're the same, so no big deal
					temp = (rand.nextInt() % 3) + 1;
				}
				if(temp > max)
				{
					max = temp;
				}
			}
			else if(color[2] == 0)
			{
				if(color[0] != color[1])
				{
					//they're different, so its better!
					temp = (rand.nextInt() % 3) + 100;
				}
				else
				{
					//they're the same, so no big deal
					temp = (rand.nextInt() % 3) + 1;
				}
				if(temp > max)
				{
					max = temp;
				}
			}
			else
			{
				if(0 > max)
				{
					max = 0;
				}; //no empties?
			}
		}
		if(Player)
		{
			return max;
		}
		else
		{
			return (-1 * max);
		}
	}

}
