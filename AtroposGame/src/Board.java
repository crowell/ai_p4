import java.util.ArrayList;
import java.util.Collections;


public class Board 
{
	public ArrayList<ArrayList<Integer> > board;
	public ArrayList<Integer> prevMove;
	
	/**
	 * Constructs the board given by the game in strings
	 * @param gameBoard
	 */
	public Board(String gameBoard)
	{
		//parse the string, add the variables to the ArrayList
		board = new ArrayList<ArrayList<Integer> >();
		prevMove = new ArrayList<Integer>();
		String[] parseboard = gameBoard.split("]");
		for(int ii = 0; ii < parseboard.length - 1; ii++)
		{
			ArrayList<Integer> row = new ArrayList<Integer>();
			for(int jj = 1; jj < parseboard[ii].length(); jj++)
			{
				String character = parseboard[ii].substring(jj, jj+1);
				int chparsed = Integer.parseInt(character);
				row.add(chparsed);
			}
			board.add(row);
		}
		Collections.reverse(board);
		if(!gameBoard.endsWith("ull"))
		{
			String[] lMoveParse = parseboard[parseboard.length - 1].split(":");
			String[] lMoveParse2 = lMoveParse[1].split(",");
			lMoveParse2[0] = lMoveParse2[0].substring(1);
			lMoveParse2[lMoveParse2.length - 1].replace(')', ' ');
			lMoveParse2[lMoveParse2.length - 1] = lMoveParse2[lMoveParse2.length - 1].substring(0, lMoveParse2[lMoveParse2.length -1].length() - 1);		
			for(int ii = 0; ii < lMoveParse2.length; ii++)
			{
				int chparsed = Integer.parseInt(lMoveParse2[ii]);
				prevMove.add(chparsed);
			}
		}
	}
	/**
	 * Constructs a board given an other board and a move
	 * @param otherboard the parent board
	 * @param x X position
	 * @param y Y position
	 * @param z Z position
	 * @param color color of the move
	 */
	public Board(Board otherboard, int x, int y, int z, int color)
	{
		//make a deep copy and put in the position and color
		board = new ArrayList<ArrayList<Integer> >();
		prevMove = new ArrayList<Integer>();
		for(int ii = 0; ii < otherboard.board.size(); ii++)
		{
			ArrayList<Integer> row = new ArrayList<Integer>();
			for(int jj = 0; jj < otherboard.board.get(ii).size(); jj++)
			{
				row.add(otherboard.board.get(ii).get(jj));
			}
			board.add(row);
		}
		//now set the move made
		int height = this.board.size();
		board.get(x).set(y, color); 
		prevMove.add(color);
		prevMove.add(x);
		prevMove.add(y);
		prevMove.add(z);
	}

}
