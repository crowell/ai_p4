
import java.util.ArrayList;

/**
 * @author Jeffrey Crowell
 * @version 0.01
 *
 */
public class jPlayer 
{

	/**
	 * @param args
	 * 
	 */
	public static void main(String[] args)
	{
		int horizonDepth = 1;
		boolean player = true;
		String br = args[0];
		//String br = "[13][302][1003][30002][100003][3000002][100002]LastPlay:null";
		Node node;
		node = new Node(br);
		ArrayList<Node> possibleMoves = node.getChildren(true);
		int bestMove = 0;
		int bestScore = Integer.MIN_VALUE;
		
		for(int ii = 0; ii < possibleMoves.size(); ii++)
		{
		    AlphaBeta ab = new AlphaBeta();
			int score = ab.Evaluate(possibleMoves.get(ii), horizonDepth, Integer.MIN_VALUE, Integer.MAX_VALUE, player);
			if(score > bestScore)
			{
				bestMove = ii;
				bestScore = score;
			}
		}
		
		//we've found the best possible move, just print it out!
		int color = possibleMoves.get(bestMove).board.prevMove.get(0);
		int x = possibleMoves.get(bestMove).board.prevMove.get(1);
		int y = possibleMoves.get(bestMove).board.prevMove.get(2);
		int z = possibleMoves.get(bestMove).board.prevMove.get(3);
		String myMove = "(" + color + "," + x + "," + y + "," + z + ")";
		System.out.print(myMove);
	}

}
