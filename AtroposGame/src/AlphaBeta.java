/**
 * @author Jeffrey Crowell
 */

import java.util.ArrayList;

public class AlphaBeta 
{
	//data members
	boolean maxPlayer = true;
	//member functions
	
	public AlphaBeta()
	{
		//construct
	}
	
	/**
	 * MiniMax with AB Pruning, based on Wikipedia's Pseudocode
	 * @param node the current node
	 * @param depth the lookahead depth
	 * @param alpha Alpha
	 * @param beta Beta
	 * @param Player maxPlayer is true, min = false
	 * @return the Score of the move
	 */
	public int Evaluate(Node node, int depth, int alpha, int beta, boolean Player)
	{
		boolean finishingMove = node.isFinal(Player);
		if(depth == 0 || finishingMove)
		{
			return node.getScore(Player, finishingMove);
		}
		if(Player == maxPlayer)
		{
			ArrayList<Node> children = node.getChildren(Player);
			for(Node child : children)
			{
				alpha = Math.max(alpha, Evaluate(child, depth - 1, alpha, beta, !(Player)));
				if(beta < alpha)
				{
					break;
				}
			}
			return alpha;
		}
		else
		{
			ArrayList<Node> children = node.getChildren(Player);
			for(Node child : children)
			{
				beta = Math.min(beta, Evaluate(child, depth - 1, alpha, beta, !(Player)));
				if(beta < alpha)
				{
					break;
				}
			}
			return beta;
		}
	}
}
